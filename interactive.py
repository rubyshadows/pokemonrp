import config
from world import World
w = World()
w.load_map(config.MAP)
w.load_characters(config.CHARACTERS)
w.print_map()