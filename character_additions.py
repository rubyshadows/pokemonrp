#These are proposed additions to character.py 
#the random module will be needed for some of the new methods, so I would import it at the top of the file 

import random 
from random import randint

# importing all of the Pokemon subclasses
from pokemon import *

#The following attributes are proposed additions to the __init__ method for the PokemonTrainer class. They will be useful for battles. 
self.pokemon_list = []
self.selected_pokemon = None
self.opponent = None
self.currently_fighting = False


#The following methods are proposed additions to the body of the PokemonTrainer class.

#This method adds a Pokémon with a specified name, type, and species to the list of Pokémon available to a trainer for battles. 
#This method currently only allows a trainer to pick one type for their Pokémon; it could later be modified to allow for one Pokémon to be of multiple types.
 
def add_pokemon(self, pokemon_name, pokemon_type, pokemon_species):

    # @Ringz: Add types map to use as a reference 
    all_types = {
        "Bug":BugPokemon, "Dark":DarkPokemon, 
        "Dragon":DragonPokemon, "Electric":ElectricPokemon, 
        "Fairy":FairyPokemon, "Fighting":FightingPokemon, 
        "Fire":FirePokemon, "Flying":FlyingPokemon, 
        "Ghost":GhostPokemon, "Grass":GrassPokemon, 
        "Ground":GroundPokemon, "Ice":IcePokemon, 
        "Normal":NormalPokemon, "Poison":PoisonPokemon, 
        "Psychic":PsychicPokemon, "Rock":RockPokemon, 
        "Steel":SteelPokemon, "Water":WaterPokemon
    }

    if pokemon_type not in all_types:
        print("Error: " + pokemon_type + " is not a type of Pokémon" )
    else:
        new_pokemon = all_types[pokemon_type](pokemon_name, pokemon_type, pokemon_species)
        self.pokemon_list.append(new_pokemon)
        print(pokemon_name + " successfully added!" )

#This method can be used by a trainer to select a Pokémon (that has been previously added to that trainer's pokemon_list) for a battle.  

def select_pokemon(self, pokemon_name):
    for i in range(len(self.pokemon_list)):
        if self.pokemon_list[i].name == pokemon_name:
            self.selected_pokemon = self.pokemon_list[i]
            print(self.name + "has selected " + self.selected_pokemon.name + " ( " + self.selected_pokemon.species + ", " + self.selected_pokemon.type + " Type) for battle" )
            break 
    if self.selected_pokemon == None:
        print("Error: This is not the name of one of your Pokémon" )

#Once two trainers have agreed to battle and selected their Pokémon, this method initiates the battle and randomly selects a player to make the first move. 

def start_battle(self):
    if self.opponent == None:
        print("Error: You do not have an opponent" )
    elif self.selected_pokemon == None:
        print("Error: You have not yet selected a Pokémon for this battle")
    elif self.opponent.selected_pokemon == None:
        print("Error: Your opponent has not yet selected a Pokémon for this battle" )
    else:
        self.currently_fighting = True
        self.opponent.currently_fighting = True
        print("Battle between " + self.name + " and " + self.opponent.name + ". " + self.name + " will be fighting with " + self.selected_pokemon.name + " ( " + self.selected_pokemon.species + ", " + self.selected_pokemon.type + " Type), and" + self.opponent.name + " will be fighting with " + self.opponent.selected_pokemon.pokemon_name + " ( " + self.opponent.selected_pokemon.pokemon_species + ", " + self.opponent.selected_pokemon.pokemon_type + " Type)" )
        random_number = random.randint(1,2)
        if random_number == 1:
            self.my_turn = True
            self.opponent.my_turn = False
            print(self.name + " makes the first move!" )
        else:
            self.my_turn = False
            self.opponent.my_turn = True
            print(self.opponent.name + " makes the first move!" )

#Use this method to attack your opponent in a battle. 
#Your attack has a 75% chance of succeeding if your Pokémon is strong against Pokémon of the type of your opponent’s Pokémon. It has a 25% chance of succeeding if your Pokémon is weak
#against that type. It has a 50% chance of success otherwise. If your attack succeeds, the hp of your opponents Pokémon goes down by 1. 
#If your opponent’s hp goes to 0, the battle is ended, and you are declared the winner. Otherwise, it is your opponent's turn to make a move. 
#There is no need to specify one's opponent as an argument since that information is already stored in self.opponent, which is referenced in the body of this method. 

def attack(self):
    if self.currently_fighting == False:
        print("Error: You are not fighting a battle right now")
    elif self.my_turn == False:
        print("Error: It is not your turn to make a move in this battle" )
    elif self.opponent.selected_pokemon.type in self.selected_pokemon.strong_against:
        random_number = randint(1, 4)
        if random_number <= 3:
            attack_successful()
        else:
            attack_missed()
    elif self.opponent.selected_pokemon.type in self.selected_pokemon.weak_against:
        random_number = randint(1,4)
        if random_number == 1:
            attack_successful()
        else:
            attack_missed()
    else:
        random_number = randint(1, 2)
        if random_number == 1:
            attack_successful()
        else:
            attack_missed()

    # @Ringz: Move repetitive tasks to functions
    def attack_successful():
        print("Attack successful!" + self.opponent.selected_pokemon.name + " loses 1 hp point" )
        self.opponent.selected_pokemon.hp -= 1 
        if self.opponent.selected_pokemon.hp > 0:
            self.my_turn = False
            self.opponent.my_turn = True
            print("Your move, " + self.opponent.name + "!" )
        else:
                print(self.name + " has won the battle!")
                self.selected_pokemon.hp = 5
                self.opponent.selected_pokemon.hp = 5
                self.currently_fighting = False
                self.opponent.currently_fighting = False
                self.selected_pokemon = None
                self.opponent.selected_pokemon = None
                self.opponent.opponent = None
                self.opponent = None 

    def attack_missed():
        print("Attack missed!" )
        self.my_turn = False
        self.opponent.my_turn = True
        print("Your move, " + self.opponent.name + "!")
