from time import sleep

class World:

    def __init__(self):
        self.map = None
        self.areas = None
        self.characters = None
        self.turn = 0

    # creates key for a dictionary of points
    def make_key(self, x, y):
        return str(x) + '_' + str(y)

    def make_key_character(self, x, y):
        return self.make_key(x,y) + '_' + 'characters'

    def character_symbol(self, name):
        char = self.characters.get(name)
        if char:
            return char.get('symbol')


    # possible errors:
        # File not found - ModuleNotFoundError
        # Classname not found in file - AttributeError
        # Class initiation is error prone - TypeError
    def instantiate_character(self, character):
        pyfilename = character.get('pyfile')
        classname = character.get('classname')
        args = character.get('args')
        try:
            char_module = __import__(pyfilename.split('.')[0])
            eval_string = 'char_module.' + classname
            obj = eval(eval_string)(**args)
            return obj
        except ModuleNotFoundError as me:
            print('instantiate character: ' + str(me))
            print('Pyfile may be missing or misspelled for '+character.get('name'))
        except AttributeError as ae:
            print('instantiate character: ' + str(ae))
            print('Class may be missing from pyfile for '+character.get('name'))
        except TypeError as te:
            print('instantiate character: ' + str(te))
            print('May have failed to input arguments into the class constructor for '+character.get('name'))
        

    def load_characters(self, characters):
        if not characters: return
        self.characters = {}
        character_symbols = []
        redundancy_count = 0
        redundancy_count_sym = 0
        for char in characters:
            name = char.get('name')
            x = char.get('x')
            y = char.get('y')
            pyfile = char.get('pyfile')
            classname = char.get('classname')
            args = char.get('args')
            if not pyfile:
                char['pyfile'] = 'ace_trainer.py'
            if not classname:
                char['classname'] = 'AceTrainer'
            if not args: char['args'] = {}
            if not name: 
                name = 'Unknown'
                char['name'] = name
            if not x: char['x'] = 0
            if not y: char['y'] = 0
            if name in self.characters:
                print('Character Error: Name {} already taken.'.format(name))
                name += redundancy_count
                char['name'] = name
                redundancy_count += 1
            symbol = name[0:2]
            if symbol in character_symbols:
                symbol += redundancy_count_sym
                redundancy_count_sym += 1
            character_symbols.append(symbol)
            
            character = self.instantiate_character(char)
            if character:
                character._id = name
                self.characters[name] = {}
                self.characters[name]['character'] = character
                self.characters[name]['symbol'] = symbol
                self.characters[name]['location'] = (x,y)
                key = self.make_key_character(x, y)
                if not self.areas.get(key): 
                    self.areas[key] = []
                self.areas[key].append(name)


    # unpacks map data
    def load_map(self, gamemap):
        if not gamemap: return
        self.map = gamemap
        self.map_name = gamemap.get('name')
        self.map_width = gamemap.get('width')
        self.map_height = gamemap.get('height')
        self.areas = {}
        areas = gamemap.get('areas')
        symbols = []
        redundancy_count = 0
        for area in areas:
            name = area.get('name')
            types = area.get('type')
            symbol = ''
            for word in name.split():
                symbol += word[0]
            if symbol in symbols:
                symbol += redundancy_count
                redundancy_count += 1
            symbols.append(symbol)
            area['symbol'] = symbol
            if 'wild' not in types:
                x = area.get('x')
                y = area.get('y')
                key = self.make_key(x, y)
                self.areas[key] = area
            else:
                ranges = area.get('ranges')
                for ran in ranges:
                    xstart = ran.get('start').get('x')
                    ystart = ran.get('start').get('y')
                    xend = ran.get('end').get('x')
                    yend = ran.get('end').get('y')
                    xpoints = []
                    ypoints = []
                    for xpoint in list(range(xstart, xend + 1)):
                        xpoints.append(xpoint)
                    for ypoint in list(range(ystart, yend + 1)):
                        ypoints.append(ypoint)
                    for i in range(len(xpoints)):
                        for j in range(len(ypoints)):
                            xpoint = xpoints[i]
                            ypoint = ypoints[j]
                            key = self.make_key(xpoint, ypoint)
                            self.areas[key] = area

    def print_map(self):
        if not self.map:
            print('World: No map loaded')
        else:
            # size, areas
            # list the area names and symbols
            self.print_map_data()
            self.print_map_matrix()

    def print_map_data(self):
        '''
            Print the names of locations and their symbols
            Each location gets a unique symbol
        '''
        area_name_symbols = {}
        char_name_symbols = {}
        for x in range(self.map_width):
            for y in range(self.map_height):
                key = self.make_key(x, y)
                key_c = self.make_key_character(x, y)
                area = self.areas.get(key)
                area_characters = self.areas.get(key_c)
                if area:
                    area_name_symbols[area.get('name')] = area.get('symbol')
                if area_characters and len(area_characters):
                    for character in area_characters:
                        char_name_symbols[character] = self.characters[character].get('symbol')
        for area_name in area_name_symbols.keys():
            print('\t'+area_name+' - '+area_name_symbols[area_name])
        print()
        for character in char_name_symbols.keys():
            char_obj = self.characters[character].get('character')
            x, y = self.location_of(char_obj)
            print('\t'+character+' - '+char_name_symbols[character]+' - '+str(x) +', '+str(y))

    def print_symbol(self, symbol=None, fill='.'):
        space_per_square = 3
        if symbol:
            spaces = space_per_square - len(symbol)
            print(fill * spaces, end='')
            print(symbol, end='')
        else:
            print(fill * space_per_square, end='')

    def print_map_matrix(self):
        print()
        for y in range(self.map_height):
            for x in range(self.map_width):
                key = self.make_key(x, y)
                key_c = self.make_key_character(x, y)
                area = self.areas.get(key)
                area_characters = self.areas.get(key_c)
                if area:
                    symbol = area.get('symbol')
                    self.print_symbol(symbol)
                elif area_characters:
                    name = area_characters[0]
                    symbol = self.character_symbol(name)
                    self.print_symbol(symbol)
                else:
                    self.print_symbol()
            print(' '+str(y), end='')
            print()
        for x in range(self.map_width):
            self.print_symbol(str(x), ' ')
        print()

    def add(self, character, x, y):
        pass

    def next_turn(self):
        '''
            Moves things to the next turn.
            Lets every character decide what to do next
        '''
        for char_name in self.characters.keys():
            char = self.characters[char_name].get('character')
            char.decide(self)
        self.turn += 1

    def location_of(self, character): # must change
        name = character._id
        if not self.characters.get(name): return None
        x, y = self.characters[name]['location']
        return (x, y)

    def update_location(self, character, new_x, new_y): # must change
        name = character._id
        x, y = self.characters[name]['location']
        key = self.make_key_character(x, y)
        new_key = self.make_key_character(new_x, new_y)
        self.areas[key].remove(name)
        self.characters[name]['location'] = (new_x, new_y)
        if self.areas.get(new_key) is None:
            self.areas[new_key] = []
        self.areas[new_key].append(name)