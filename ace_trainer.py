from character import PokemonTrainer, Character

class AceTrainer(PokemonTrainer):
    def __init__(self, name):
        PokemonTrainer.__init__(self, name)

class Gillian(AceTrainer):
    def __init__(self): # takes no arguments
        AceTrainer.__init__(self, 'Gillian')
        self._has_introduced = False

    def decide(self, world):
        super().decide(world)
        if not self._has_introduced:
            self._talk('Welcome to the Pokemon RP!')
            self._has_introduced = True

class Misty(AceTrainer):
    def __init__(self): 
        AceTrainer.__init__(self, 'Misty')