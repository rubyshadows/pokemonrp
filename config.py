'''
CONFIG OVERVIEW:
    config.py tells the program:
        1. Where to find Character subclass files.
        2. How to shape and populate the map.

    So in this iteration you must edit this file to add characters to the scenario.

CONFIG FILE STRUCTURE:
    Characters Array
        1. This is an array of characters that will spawn at one of the specified spawn points.
        2. Each character requires a name, pyfile, and classname (found in the pyfile)

    Constraints
        1. Trainers can only carry up to 6 pokemon
        2. Any pokemon beyond the 6th, initialized in the trainer's pyfile or caught in game, will be sent to storage

    Objectives
        1. Beat all of the gyms on the map
        2. Beat all of the trainers on the map
        3. Catch all of the pokemon on the map
            - The first trainer to accomplish an objective will recieve a title
                - Trainer - First to beat all gyms
                - Fighter - First to beat all other trainers
                - Catcher - First to catch all available pokemon

    Map Dictionary
        1. This dictionary defines the shape of the map, its buildings, and its wild encounters areas.
        2. It's attributes are:
            a. name - the name of the map
            b. size - the map will be (size x size) 
            c. areas - an array of special areas on the map. Each area has:
                i. name
                ii. x and y position
                iii. type - an array of types that define this area
                    - areas containing type 'heal' heal all of your party pokemon when entered
                    - areas containing type 'store' calls the trainer's 'store' function
                    - areas containing type 'work' calls the trainer's 'work' function
                    - areas containing type 'gym' calls the trainer's 'gym' function
                        - a gym containers a trainer
                    - areas containing type 'wild' starts a battle with a wild pokemon
                        - wild areas can have a range of x and y values, e.g. a long strip of grass   
                        - wild areas can contain day_pokemon and night_pokemon
                            - each pokemon's appearance chance is influenced by its 'percentage' 
                            - day is between 7am - 7pm, while night is from 7pm - 7am
                            - pokemon can be caught with pokeballs bought at the mart
'''
##################### Characters #################
CHARACTERS = [ # Here's two examples of adding characters
    {
        'name': 'Gillian', #1
        'pyfile': 'ace_trainer.py', #check this file for Gillian's code
        'classname': 'Gillian',
        'args': {},
        'x': 2,
        'y': 3,
        'pokemon': [
        {
            'name': 'Ruby',
            'species': 'Meganium',
            'type': 'Grass'
        },
        {
            'name': 'Pokey',
            'species': 'Pidgey',
            'type': 'Flying'
        }]
    },
    {
        'name': 'Iloria', #2
        'pyfile': 'iloria.py',
        'classname': 'Iloria',
        'args': {},
        'x':4,
        'y':9,
        'pokemon':[
        {
            'name': 'Altioran',
            'species': 'Slowpoke',
            'type': 'Water'
        },
        {
            'name': 'Sempiterna',
            'species': 'Houndour',
            'type': 'Dark'
        },
        {
            'name': 'Tacitus',
            'species': 'Omanyte',
            'type': 'Rock'
        }]
    },
    {
        'name': 'Misty',
        'pyfile': 'ace_trainer.py',
        'classname': 'Misty',
        'args': {},
        'x':4,
        'y':5,
        'pokemon': [
        {
            'name': 'Starmie',
            'species': 'Starmie',
            'type': 'Water'
        },
        {
            'name': 'Laplace',
            'species': 'Lapris',
            'type': 'Ice'
        },
        {
            'name': 'Sunnygo',
            'species': 'Corsola',
            'type': 'Rock'
        },
        {
            'name': 'Koduck',
            'species': 'Psyduck',
            'type': 'Water'
        }]
    },
    {
        'name': 'Aldred', #2
        'pyfile': 'iloria.py',
        'classname': 'Aldred',
        'args': {},
        'x':5,
        'y':9,
        'pokemon':[
        {
            'name': 'Abra',
            'species': 'Abra',
            'type': 'Psychic'
        },
        {
            'name': 'Ditto',
            'species': 'Ditto',
            'type': 'Normal'
        }]
    },
    {
        'name': 'Danny', #2
        'pyfile': 'ace_trainer.py',
        'classname': 'AceTrainer',
        'args': {'name': 'Danny'},
        'x':8,
        'y':1,
        'pokemon':[
        {
            'name': 'Yura',
            'species': 'Lopunny',
            'type': 'Normal'
        }
        ]
    }

]

################## MAP ####################

MAP = {
    'name': 'Cerulean City',
    'width': 10,
    'height': 10,
    'areas':[
        {
            'name': 'Pokemon Center',
            'type':['heal','work'],
            'x': 6,
            'y': 6
        },
        {
            'name': 'PokeMart',
            'type': ['store', 'work'],
            'x': 3,
            'y': 6
        },
        {
            'name': 'Cerulean City Gym',
            'type': ['gym', 'work'],
            'x': 1,
            'y': 1,
            'trainer': 'Misty'
        },
        {
            'name': 'Autumn Inn',
            'type':['heal', 'spawn'],
            'x': 8,
            'y': 9
        },
        {
            'name': 'Grass',
            'ranges':[
            {
                'start': {'x': 4, 'y': 3},
                'end': {'x': 6, 'y': 3}
            },
            {
                'start': {'x': 0, 'y': 9},
                'end': {'x': 2, 'y': 9}
            }
            ],
            'type': ['wild'],
            'day_pokemon':[
            {
                'species': 'Pidgey',
                'type': 'Flying',
                'percent': 30
            },
            {
                'species': 'Pineco',
                'type': 'Bug',
                'percent': 15
            },
            {
                'species': 'Rattata',
                'type': 'Normal',
                'percent': 35
            },
            {
                'species': 'Bulbasaur',
                'type': 'Grass',
                'percent': 5
            },
            {
                'species': 'Oddish',
                'type': 'Grass',
                'percent': 15
            }],
            'night_pokemon':[
            {
                'species': 'Gastly',
                'type': 'Ghost',
                'percent': 15
            },
            {
                'species': 'Hoothoot',
                'type': 'Flying',
                'percent': 30
            },
            {
                'species': 'Zubat',
                'type': 'Flying',
                'percent': 25
            },
            {
                'species': 'Ekans',
                'type': 'Poison',
                'percent': 20
            },
            {
                'species': 'Mankey',
                'type': 'Fighting',
                'percent': 10
            }]
        }
    ]
}