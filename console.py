from cmd import Cmd
import config
from world import World

class PokemonRP(Cmd):
    prompt = '(PkmnRP) '
    def __init__(self):
        Cmd.__init__(self)
        self.world = World()
        self.world.load_map(config.MAP)
        self.world.load_characters(config.CHARACTERS)
        self.world.print_map()
        self.onecmd('?')

    def emptyline(self):
        # Do nothing on empty input
        return False

    def do_quit(self, args):
        '''Quit program'''
        return True

    def do_step(self, args):
        '''Step forward x many turns'''
        if not args: args = 1
        try:
            steps = int(args)
            for i in range(steps):
                self.world.next_turn()
            self.world.print_map_matrix()
        except:
            print('Usage: step <number>')

    def do_map(self, args):
        '''Prints map data'''
        self.world.print_map()

if __name__ == '__main__':
    PokemonRP().cmdloop()