class Pokemon:
    def __init__(self, name, type, species):
        self.hp = 5 
        self.name = name
        self.type = type
        self.species = species 


class BugPokemon(Pokemon):
    def __init__(self, name, type = "Bug", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Dark", "Grass", "Psychic"]
        self.weak_against = ["Fighting", "Flying", "Poison", "Ghost", "Steel", "Fire", "Fairy"] 

class DarkPokemon(Pokemon):
    def __init__(self, name, type = "Dark", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Ghost", "Psychic"]
        self.weak_against = ["Dark", "Fairy", "Fighting"] 

class DragonPokemon(Pokemon):
    def __init__(self, name, type = "Dragon", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Dragon"]
        self.weak_against = ["Steel", "Fairy"] 

class ElectricPokemon(Pokemon):
    def __init__(self, name, type = "Electric", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Flying", "Water"]
        self.weak_against = ["Ground", "Grass", "Electric", "Dragon"] 

class FairyPokemon(Pokemon):
    def __init__(self, name, type = "Fairy", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Fighting", "Dragon", "Dark"]
        self.weak_against = ["Poison", "Steel", "Fire"] 

class FightingPokemon(Pokemon):
    def __init__(self, name, type = "Fighting", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Normal", "Rock", "Steel", "Ice", "Dark"]
        self.weak_against = ["Flying", "Poison", "Psychic", "Bug", "Ghost", "Fairy"] 

class FirePokemon(Pokemon):
    def __init__(self, name, type = "Fire", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Bug", "Steel", "Grass", "Ice"]
        self.weak_against = ["Rock", "Fire", "Water", "Dragon"]

class FlyingPokemon(Pokemon):
    def __init__(self, name, type = "Flying", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Fighting", "Bug", "Grass"]
        self.weak_against = ["Rock", "Steel", "Electric"] 

class GhostPokemon(Pokemon):
    def __init__(self, name, type = "Ghost", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Ghost", "Psychic"]
        self.weak_against = ["Normal", "Dark"] 

class GrassPokemon(Pokemon):
    def __init__(self, name, type = "Grass", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Ground", "Rock", "Water"]
        self.weak_against = ["Flying", "Poison", "Bug", "Steel", "Fire", "Grass", "Dragon"] 

class GroundPokemon(Pokemon):
    def __init__(self, name, type = "Ground", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Poison", "Rock", "Steel", "Fire", "Electric"]
        self.weak_against = ["Flying", "Bug", "Grass"] 

class IcePokemon(Pokemon):
    def __init__(self, name, type = "Ice", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Flying", "Ground", "Grass", "Dragon"]
        self.weak_against = ["Steel", "Fire", "Water", "Ice"] 

class NormalPokemon(Pokemon):
    def __init__(self, name, type = "Normal", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = []
        self.weak_against = ["Rock", "Ghost", "Steel"]

class PoisonPokemon(Pokemon):
    def __init__(self, name, type = "Poison", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Grass", "Fairy"]
        self.weak_against = ["Poison", "Ground", "Rock", "Ghost", "Steel"] 

class PsychicPokemon(Pokemon):
    def __init__(self, name, type = "Psychic", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Fighting", "Poison"]
        self.weak_against = ["Steel", "Psychic", "Dark"]

class RockPokemon(Pokemon):
    def __init__(self, name, type = "Rock", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Flying", "Bug", "Fire", "Ice"]
        self.weak_against = ["Fighting", "Ground", "Steel"] 

class SteelPokemon(Pokemon):
    def __init__(self, name, type = "Steel", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Rock", "Ice", "Fairy"]
        self.weak_against = ["Steel", "Fire", "Water", "Electric"]

class WaterPokemon(Pokemon):
    def __init__(self, name, type = "Water", species = "Unknown"):
        Pokemon.__init__(self, name, type, species)
        self.strong_against = ["Ground", "Rock", "Fire"]
        self.weak_against = ["Water", "Grass", "Dragon"]
