import random 
from random import randint

# importing all of the Pokemon subclasses
from pokemon import *

# Base class that might be used for NPCs that don't battle
class Character:
    def __init__(self, name):
        self._name = name

    def name(self):
        return self._name

# Class for characters that use Pokemon
class PokemonTrainer(Character):
    def __init__(self, name):
        Character.__init__(self, name)

        self._pokemon_list = []
        self._selected_pokemon = None
        self._opponent = None
        self._currently_fighting = False
        self._my_turn = False

    def set_turn(self, opponent):
        if opponent == self._opponent:
            self._my_turn = True

    def opponent(self):
        return self._opponent

    # This method is meant to be overwritten by subclasses
    # In this example, the response is always true via the self.accept method
    def will_accept(self, opponent):
        if self._opponent is not None:
            true_or_false = self._decline(opponent)
        else:
            true_or_false = self._accept(opponent)
        return true_or_false

    # Meant to be overwritten
    # This method gets called each turn, and it is how the character decides what to do 
    def decide(self, world):
        if self._opponent is not None:
            self._talk("(Continues to battle "+self._opponent.name+")")
            return
        x, y = world.location_of(self)
        if x > y:
            new_x = x - 1
            new_y = y
        elif x < y:
            new_x = x
            new_y = y - 1
        elif x >= world.map_width - 1:
            new_x = x - 1
            new_y = y
        elif y >= world.map_height - 1:
            new_x = x
            new_y = y - 1
        elif x <= 0:
            new_x = x + 1
            new_y = y 
        elif y <= 0:
            new_x = x
            new_y = y + 1
        else:
            new_x = x + 1
            new_y = y + 1        
        self._move(world, new_x, new_y)

    def _move(self, world, new_x, new_y):
        world.update_location(self, new_x, new_y)

    # Appends name before print statements
    def _talk(self, msg):
        print(self._name + ": "+msg)

    # Calls a potential opponent's "will_accept" method
    # sets self._opponent equal to that opponent if response is True
    def _challenge(self, opponent):
        print(self.name() + " challenges " + opponent.name() + " to a battle.")
        response = opponent.will_accept(self)
        if response == True:
            self._opponent = opponent
        return response

    # Helper method used by 'self.will_accept(opponent)' method
    def _accept(self, opponent):
        self._talk("Sure, "+opponent.name+"! Lets have a match.")
        self._opponent = opponent
        return True

    # Helper method used by 'self.will_accept(opponent)' method
    def _decline(self, opponent):
        self._talk("Sorry "+opponent.name+". Not right now.")
        return False

    def _add_pokemon(self, pokemon_name, pokemon_type, pokemon_species):
        
        # @Ringz: Add all_types map to use as a reference 
        all_types = {
            "Bug":BugPokemon, "Dark":DarkPokemon, 
            "Dragon":DragonPokemon, "Electric":ElectricPokemon, 
            "Fairy":FairyPokemon, "Fighting":FightingPokemon, 
            "Fire":FirePokemon, "Flying":FlyingPokemon, 
            "Ghost":GhostPokemon, "Grass":GrassPokemon, 
            "Ground":GroundPokemon, "Ice":IcePokemon, 
            "Normal":NormalPokemon, "Poison":PoisonPokemon, 
            "Psychic":PsychicPokemon, "Rock":RockPokemon, 
            "Steel":SteelPokemon, "Water":WaterPokemon
        }

        if pokemon_type not in all_types:
            print("Error: " + pokemon_type + " is not a type of Pokémon" )
        else:
            new_pokemon = all_types[pokemon_type](pokemon_name, pokemon_type, pokemon_species)
            self._pokemon_list.append(new_pokemon)
            print(pokemon_name + " successfully added!" )

    #This method can be used by a trainer to select a Pokémon 
    # (that has been previously added to that trainer's pokemon_list) for a battle.  

    def _select_pokemon(self, pokemon_name):
        for i in range(len(self._pokemon_list)):
            if self._pokemon_list[i].name == pokemon_name:
                self._selected_pokemon = self._pokemon_list[i]
                print(self.name()+ "has selected " + self._selected_pokemon.name + " ( " + self._selected_pokemon.species + ", " + self._selected_pokemon.type + " Type) for battle" )
                break 
        if self._selected_pokemon == None:
            print("Error: This is not the name of one of your Pokémon" )

    #Once two trainers have agreed to battle and selected their Pokémon, 
    # this method initiates the battle and randomly selects a player to make the first move. 

    def _start_battle(self):
        if self._opponent == None:
            print("Error: You do not have an opponent" )
        elif self._selected_pokemon == None:
            print("Error: You have not yet selected a Pokémon for this battle")
        elif self._opponent.selected_pokemon == None: #unsettable
            print("Error: Your opponent has not yet selected a Pokémon for this battle" )
        else:
            self._currently_fighting = True
            self._opponent.currently_fighting = True # unsettable
            print("Battle between " + self.name()+ " and " + self._opponent.name + ". " + self.name()+ " will be fighting with " + self._selected_pokemon.name + " ( " + self._selected_pokemon.species + ", " + self._selected_pokemon.type + " Type), and" + self._opponent.name + " will be fighting with " + self._opponent.selected_pokemon.pokemon_name + " ( " + self._opponent.selected_pokemon.pokemon_species + ", " + self._opponent.selected_pokemon.pokemon_type + " Type)" )
            random_number = random.randint(1,2)
            if random_number == 1:
                self._my_turn = True
                # self._opponent.my_turn = False
                print(self.name()+ " makes the first move!" )
            else:
                self._my_turn = False
                self._opponent.set_turn()
                print(self._opponent.name + " makes the first move!" )

    #Use this method to attack your opponent in a battle. 
    #Your attack has a 75% chance of succeeding if your Pokémon is strong against Pokémon of the type of your opponent’s Pokémon. It has a 25% chance of succeeding if your Pokémon is weak
    #against that type. It has a 50% chance of success otherwise. If your attack succeeds, the hp of your opponents Pokémon goes down by 1. 
    #If your opponent’s hp goes to 0, the battle is ended, and you are declared the winner. Otherwise, it is your opponent's turn to make a move. 
    #There is no need to specify one's opponent as an argument since that information is already stored in self._opponent, which is referenced in the body of this method. 

    def _attack(self):
        if self._currently_fighting == False:
            print("Error: You are not fighting a battle right now")
        elif self._my_turn == False:
            print("Error: It is not your turn to make a move in this battle" )
        elif self._opponent.selected_pokemon.type in self._selected_pokemon.strong_against:
            random_number = randint(1, 4)
            if random_number <= 3:
                attack_successful()
            else:
                attack_missed()
        elif self._opponent.selected_pokemon.type in self._selected_pokemon.weak_against:
            random_number = randint(1,4)
            if random_number == 1:
                attack_successful()
            else:
                attack_missed()
        else:
            random_number = randint(1, 2)
            if random_number == 1:
                attack_successful()
            else:
                attack_missed()

        # @Ringz: Move repetitive tasks to functions
        def attack_successful():
            print("Attack successful!" + self._opponent.selected_pokemon.name + " loses 1 hp point" )
            self._opponent.selected_pokemon.hp -= 1 
            if self._opponent.selected_pokemon.hp > 0:
                self._my_turn = False
                self._opponent.set_turn()
                print("Your move, " + self._opponent.name + "!" )
            else:
                    print(self.name()+ " has won the battle!")
                    self._selected_pokemon.hp = 5
                    self._opponent.selected_pokemon.hp = 5
                    self._currently_fighting = False
                    self._opponent.currently_fighting = False
                    self._selected_pokemon = None
                    self._opponent.selected_pokemon = None
                    self._opponent.opponent = None
                    self._opponent = None 

        def attack_missed():
            print("Attack missed!" )
            self._my_turn = False
            self._opponent.set_turn()
            print("Your move, " + self._opponent.name + "!")