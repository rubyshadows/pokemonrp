# The tasks this file performs are:
#   1. Read config.py file
#   2. Create the world object according to the config file
#   3. Execute the world object's next_turn method
#   4. Check the elements of the world each turn
#   5. Save the state of the world to a file that can be read from later
#   6. Respond to Keyboard Interrupt signal elegantly 

from character import PokemonTrainer
from world import World

melino = PokemonTrainer(name = "Melino", 
                        accept_text = "Sure, I'm game.",
                        challenge_text = "Would you spar with me?",
                        reject_text = "Not yet.")
iloria = PokemonTrainer(name = "Iloria",
                        accept_text = "Okay, if you want.",
                        challenge_text = "Would you like to battle?",
                        reject_text = "Hm. I can't right now.")
dan = PokemonTrainer(name = "Danny",
                     accept_text = "Of course.",
                     challenge_text = "Shall we fight?",
                     reject_text = "I have too many tests tomorrow.")
stuff = PokemonTrainer(name = "Shinji",
                       accept_text = "If you want.",
                       challenge_text = "Let's do this.",
                       reject_text = "No time.")
world = World()
world.add(iloria, "Fountain")
world.add(melino, "Pokemon Center")
world.add(dan, "Cerulean Gym")
world.add(stuff, "Bus Stop")
while(not world.next_turn()):
    print("Turn: "+str(world.turn))
